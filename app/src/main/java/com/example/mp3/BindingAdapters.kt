package com.example.mp3

import android.media.MediaPlayer
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("musicstatus")
fun bindMusicStatus(imageView: ImageView, isPlaying: Boolean) {
    when(isPlaying){
        true -> {
            imageView.setImageResource(R.drawable.ic_round_pause)
        }
        false-> {
            imageView.setImageResource(R.drawable.ic_round_play)
        }
    }
}

@BindingAdapter("RepeatStatus")
fun bindRepeatStatus(imageView: ImageView, status: RepeatStatus?) {
    when(status){
        RepeatStatus.REPEAT -> imageView.setImageResource(R.drawable.ic_repeat_on)
        RepeatStatus.UN_REPEAT -> imageView.setImageResource(R.drawable.ic_repeat_off)
    }
}


@BindingAdapter("ShuffleStatus")
fun bindShuffleStatus(imageView: ImageView, status: ShuffleStatus?) {
    when(status){
        ShuffleStatus.SHUFFLE -> imageView.setImageResource(R.drawable.ic_shuffle_on)
        ShuffleStatus.UN_SHUFFLE -> imageView.setImageResource(R.drawable.ic_shuffle_off)
    }
}

