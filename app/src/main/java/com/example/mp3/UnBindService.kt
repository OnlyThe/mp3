package com.example.mp3

import android.content.ServiceConnection

interface UnBindService {
    fun unBind(serviceConnection: ServiceConnection)
}