package com.example.mp3

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BroadCast: BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        val action = p1?.getIntExtra("action_music", 0)

        val intent = Intent(p0, MyService::class.java)

        intent.putExtra("action_music_service", action)

        p0?.startService(intent)
    }
}