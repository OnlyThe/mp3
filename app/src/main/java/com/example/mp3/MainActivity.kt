package com.example.mp3

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import android.widget.SeekBar
import com.example.mp3.databinding.ActivityMainBinding

enum class RepeatStatus{REPEAT, UN_REPEAT}

enum class ShuffleStatus{SHUFFLE, UN_SHUFFLE}

enum class MusicStatus{PAUSE, PLAYING}

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var myService: MyService ?= null

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val myBinder: MyService.MyBinder = p1 as MyService.MyBinder
            myService = myBinder.getService()

            myService?.isPlaying?.observe(this@MainActivity){
                if(it == true){
                    binding.imgPlay.setImageResource(R.drawable.ic_round_pause)
                }
                else{
                    binding.imgPlay.setImageResource(R.drawable.ic_round_play)
                }
            }
            myService?.mediaPlayer?.let {
                binding.seekBar.max = it.duration/1000
            }

        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            myService = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val song = Song("Phai Chang em da yeu", "Ha Linh", R.raw.phai_chang_em_da_yeu)

        startService(song)

        binding.imgPlay.setOnClickListener{

            myService?.mediaPlayer?.let { it1 -> handlePlayOrPause(it1) }
        }

        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                if(p2){
                    myService?.mediaPlayer?.let {
                        it.seekTo(p1*1000)
                    }
                }

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })

        runOnUiThread(object : Runnable {
            override fun run() {
                myService?.mediaPlayer?.let {
                    val currentPosition: Int = it.currentPosition/1000
                    binding.seekBar.progress = currentPosition
                    binding.tvTimeStart.text = formattedTime(currentPosition)

                }
                Handler(Looper.getMainLooper()).postDelayed(this, 1000)
            }

        })
    }

    private fun formattedTime(currentPosition: Int): String{
        val minutes = currentPosition/60
        val seconds = currentPosition%60
        return if(seconds < 10){
            "$minutes:0$seconds"
        } else{
            "$minutes:$seconds"
        }
    }

    private fun handlePlayOrPause(mediaPlayer: MediaPlayer){
        if(mediaPlayer.isPlaying){

            binding.imgPlay.setImageResource(R.drawable.ic_round_play)
            myService?.pauseMusic()

        }
        else{
            binding.imgPlay.setImageResource(R.drawable.ic_round_pause)
            myService?.resumeMusic()
        }
    }

    private fun startService(song: Song){


        val intent = Intent(this, MyService::class.java)

        val bundle = Bundle()
        bundle.putSerializable("song", song)

        intent.putExtras(bundle)
        startService(intent)

        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onDestroy() {
        super.onDestroy()

        unbindService(serviceConnection)
    }


}