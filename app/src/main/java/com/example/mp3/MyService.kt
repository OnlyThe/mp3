package com.example.mp3

import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class MyService : Service() {

    companion object ACTION{
        const val PAUSE = 1
        const val RESUME = 2
        const val CLOSE = 3
    }
    private lateinit var msong: Song

    lateinit var unBindService: UnBindService

    private var _isPlaying = MutableLiveData<Boolean>()
    val isPlaying: LiveData<Boolean> = _isPlaying

    var mediaPlayer: MediaPlayer ?= null

    private val myBinder = MyBinder()

    inner class MyBinder: Binder(){
        fun getService(): MyService {
            return this@MyService
        }
    }

    override fun onBind(p0: Intent?): IBinder {
        return myBinder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val bundle = intent?.extras

       bundle?.get("song")?.let {
           msong = it as Song
       }

        startMusic(msong)

        sendNotificationMedia(msong)

        val action = intent?.getIntExtra("action_music_service", 0)
        action?.let { handleActionMusic(it) }

        return START_NOT_STICKY

    }

    private fun startMusic(song: Song){
        if(mediaPlayer == null){
            mediaPlayer = MediaPlayer.create(applicationContext, song.resource)
        }
        mediaPlayer?.start()
        _isPlaying.value = true

    }

    override fun onUnbind(intent: Intent?): Boolean {
        return super.onUnbind(intent)
    }

    fun handleActionMusic(action: Int){
        when(action){
            PAUSE -> {
                _isPlaying.value = false
                pauseMusic()
            }
            RESUME -> {
                _isPlaying.value = true
                resumeMusic()

            }
            CLOSE ->{
                stopSelf()
                stopForeground(true)
            }
        }

    }

    fun pauseMusic(){
        mediaPlayer?.pause()
        sendNotificationMedia(msong)

    }

    fun resumeMusic(){
        mediaPlayer?.start()
        sendNotificationMedia(msong)

    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer?.release()
        _isPlaying.value = false

    }

    private fun getPendingIntent(context: Context, action: Int) : PendingIntent{


        val intent = Intent(this, BroadCast::class.java)

        intent.putExtra("action_music", action)

        return PendingIntent.getBroadcast(context.applicationContext, action, intent, PendingIntent.FLAG_UPDATE_CURRENT)

    }

    private fun sendNotificationMedia(song: Song) {

        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this, MyApplication.CHANNEL_ID)
            // Show controls on lock screen even when user hides sensitive content.
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setSmallIcon(R.drawable.ic_round_play)
            .setSound(null)
            // Apply the media style template
            .setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                .setShowActionsInCompactView(0,1,2 /* #1: pause button \*/))
                //.setMediaSession(mediaSessionCompat.sessionToken))
            .setContentTitle(song.name)
            .setContentText(song.single)

        if(mediaPlayer?.isPlaying == true){
            notificationBuilder
                .addAction(R.drawable.ic_previous, "Previous", null) // #0
                .addAction(R.drawable.ic_round_pause, "Pause", getPendingIntent(this, PAUSE)) // #1
                .addAction(R.drawable.ic_next, "Next", null) // #2
                .addAction(R.drawable.ic_close, "Close", getPendingIntent(this, CLOSE))
        }
        else{
            notificationBuilder
                .addAction(R.drawable.ic_previous, "Previous", null) // #0
                .addAction(R.drawable.ic_round_play, "Pause", getPendingIntent(this, RESUME)) // #1
                .addAction(R.drawable.ic_next, "Next", null) // #2
                .addAction(R.drawable.ic_close, "Close", getPendingIntent(this, CLOSE))
        }
        startForeground(1, notificationBuilder.build())
    }
}