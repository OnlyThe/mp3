package com.example.mp3

import java.io.Serializable

data class Song(val name: String, val single: String, val resource: Int) : Serializable
